window.onload = function(){
var secs = 00;
var mins = 00;
var appednTens = document.getElementById("mins");
var appednSecons = document.getElementById("secs");
var buttonStart = document.getElementById("button-start");
var buttonStop = document.getElementById("button-stop");
var buttonReset = document.getElementById("button-reset");
var intervals;

buttonStart.onclick= function() {
    intervals = setInterval(startTimer,0.001);
}

buttonStop.onclick = function () {
    clearInterval(intervals);
};

buttonReset.onclick = function () {
  clearInterval(intervals);
  secs = "00";
  mins = "00";
  appednTens.innerHTML = mins;
  appednSecons.innerHTML = secs;
};

function startTimer(){
    mins++;
    if(mins<= 9){
        appednTens.innerHTML = "0"+mins;
    }
    if(mins> 99){
        // console.log("seconds");
        secs++;
        appednSecons.innerHTML='0'+secs;
        mins=0;
        appednTens.innerHTML="0"+0;
    }
    if(secs > 9 ){
        appednSecons.innerHTML= secs;
    }
}

}